// Import thư viện Express Js
const express = require("express");
const { courseRouter } = require("./app/routes/courseRouter");
const { reviewRouter } = require("./app/routes/reviewRouter");

// Khởi tạo 1 app express
const app = express();

// Khai báo 1 cổng 
const port = 8000;

app.use('/', courseRouter);

app.use('/', reviewRouter);
// run app on declared port
app.listen(port, () => {
    console.log(`App running on port ${port}`);
});