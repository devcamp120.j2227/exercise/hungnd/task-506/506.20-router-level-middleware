const getAllReviewsMiddleware = (req, res, next) => {
    console.log("Get All Reviews!");
    next();
};

const getReviewsMiddleware = (req, res, next) => {
    console.log("Get a Reviews!");
    next();
};

const postReviewsMiddleware = (req, res, next) => {
    console.log("Create a Reviews!");
    next();
};

const putReviewsMiddleware = (req, res, next) => {
    console.log("Update a Reviews!");
    next();
};

const deleteReviewsMiddleware = (req, res, next) => {
    console.log("Delete a Reviews!");
    next();
};
module.exports  = {
    getAllReviewsMiddleware,
    getReviewsMiddleware,
    postReviewsMiddleware,
    putReviewsMiddleware,
    deleteReviewsMiddleware
}
