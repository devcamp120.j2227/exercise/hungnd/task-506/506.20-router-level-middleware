const getAllCoursesMiddleware = (req, res, next) => {
    console.log("Get All Courses!");
    next();
};

const getCoursesMiddleware = (req, res, next) => {
    console.log("Get a Courses!");
    next();
};

const postCoursesMiddleware = (req, res, next) => {
    console.log("Create a Courses!");
    next();
};

const putCoursesMiddleware = (req, res, next) => {
    console.log("Update a Courses!");
    next();
};

const deleteCoursesMiddleware = (req, res, next) => {
    console.log("Delete a Courses!");
    next();
};
module.exports  = {
    getAllCoursesMiddleware,
    getCoursesMiddleware,
    postCoursesMiddleware,
    putCoursesMiddleware,
    deleteCoursesMiddleware
}
