
// khai báo thư viện express
const express = require('express');

// Khai báo middleware
const {
    getAllCoursesMiddleware,
    getCoursesMiddleware,
    postCoursesMiddleware,
    putCoursesMiddleware,
    deleteCoursesMiddleware
} = require('../middleware/courseMiddleware');

// Tạo ra Router
const courseRouter = express.Router();
// dùng use sẽ truyền hết mỗi lần chạy
courseRouter.use(getAllCoursesMiddleware);
// dùng cho từng phương thức
courseRouter.get('/courses', getAllCoursesMiddleware, (req, res) => {
    res.json({
        message: "Get all courses"
    })
});

courseRouter.post('/courses', postCoursesMiddleware, (req, res) => {
    res.json({
        message: "Create new courses"
    })
});

courseRouter.put('/courses/:coursesId', putCoursesMiddleware, (req, res) => {
    let coursesId = req.params.coursesId;
    res.json({
        message: `Update courses id = ${coursesId}`
    })
});

courseRouter.delete('/courses/:coursesId', deleteCoursesMiddleware, (req, res) => {
    let coursesId = req.params.coursesId;
    res.json({
        message: `Delete courses id = ${coursesId}`
    })
});

courseRouter.get('/courses/:coursesId', getCoursesMiddleware, (req, res) => {
    let coursesId = req.params.coursesId;
    res.json({
        message: `Get courses id = ${coursesId}`
    })
});
module.exports = { courseRouter }